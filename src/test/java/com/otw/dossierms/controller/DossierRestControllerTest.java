package com.otw.dossierms.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.otw.dossierms.dto.DossierDTO;
import com.otw.dossierms.dto.FileDTO;
import com.otw.dossierms.model.Dossier;
import com.otw.dossierms.model.File;
import com.otw.dossierms.service.DossierService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.constraints.AssertTrue;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DossierRestControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private DossierService dossierService;

    private Dossier dossier;
    private DossierDTO dossierDTO;
    private Dossier initDossier;

    @Before
    public void init() {
        dossierService.deleteAll();
        initDossier = new Dossier(1L, Arrays.asList(new File(3L, LocalDateTime.of(2018, 4, 20, 5, 55), 4L, 49L, "Jonas zijn contactmomentje", "Niks aan")));
    }


    @Test
    public void createDossier() throws Exception {
        dossier = new Dossier(2L, null);
        dossierDTO = modelMapper.map(dossier, DossierDTO.class);

        mockMvc.perform(post("/dossier/create")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(dossierDTO)))
                .andExpect(status().isCreated());

        Assert.assertEquals(dossier.getContactId(),dossier.getContactId());

    }

    @Test
    public void updateFile() throws Exception {
        dossierService.save(initDossier);

        FileDTO fileDTO= modelMapper.map(new File(3L,LocalDateTime.now(),0L,0L,"testing", "testing") , FileDTO.class);
        fileDTO.setId(5L);

        mockMvc.perform(put("/dossier/updateFile")
                .header("User-ID", "1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(fileDTO)))
                .andExpect(status().isAccepted());

        String unexpectedNote = initDossier.getFile().get(0).getNote();
        String actualNote = dossierService.findDossier(3L).get().getFile().get(0).getNote();

        Assert.assertNotEquals(unexpectedNote,actualNote);
    }

    @Test
    public void getDossier() throws Exception {
        dossierService.save(initDossier);
        mockMvc.perform(get("/dossier/getContactDossiers/3")
                .header("User-ID", "1")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"contactId\":1,\"file\":[{\"id\":4,\"contactId\":3,\"time\":\"2018-04-20T05:55:00\",\"latitude\":4.0,\"longitude\":49.0,\"title\":\"Jonas zijn contactmomentje\",\"note\":\"Niks aan\"}]}"));

    }

    @Test
    public void deleteDossier() throws Exception {
        dossierService.save(initDossier);
        List<Dossier> dossierList = dossierService.findAll();
        mockMvc.perform(delete("/dossier/delete/" + dossierList.get(0).getId())).andExpect(status().isOk());

        Assert.assertFalse(dossierService.findDossier(3L).isPresent());
    }
}
