package com.otw.dossierms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class DossierMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DossierMsApplication.class, args);
    }

}

