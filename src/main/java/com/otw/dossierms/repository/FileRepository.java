package com.otw.dossierms.repository;

import com.otw.dossierms.model.File;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<File,Long> {

}
