package com.otw.dossierms.repository;

import com.otw.dossierms.model.Dossier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DossierRepository extends JpaRepository<Dossier, Long> {

    public void deleteAll();

    Optional<Dossier> findByContactId(Long contactId);



}
