package com.otw.dossierms.controller;

import com.otw.dossierms.dto.DossierDTO;
import com.otw.dossierms.dto.FileDTO;
import com.otw.dossierms.model.Dossier;
import com.otw.dossierms.model.File;
import com.otw.dossierms.service.DossierService;
import com.otw.dossierms.service.FileService;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/dossier")
public class DossierRestController {
    private final DossierService dossierService;
    private final ModelMapper modelMapper;
    private final FileService fileService;

    public DossierRestController(DossierService dossierService, ModelMapper modelMapper, FileService fileService) {
        this.dossierService = dossierService;
        this.modelMapper = modelMapper;
        this.fileService = fileService;
    }

    @PostMapping("/create")
    public ResponseEntity<DossierDTO> createDossier(@RequestBody DossierDTO dossierDTO) {
        Dossier dossier = modelMapper.map(dossierDTO, Dossier.class);
        Dossier dossierOut = dossierService.save(dossier);
        return new ResponseEntity<>(modelMapper.map(dossierOut, DossierDTO.class), HttpStatus.CREATED);

    }

    @GetMapping("/getContactDossiers/{contactId}")
    public ResponseEntity<DossierDTO> getContactDossiers(@PathVariable Long contactId, @RequestHeader("User-ID") Long userId) {
        Dossier dossier = dossierService.findByContactId(userId);

        dossier.setFile(dossier.getFile().stream()
                .filter(f -> f.getContactId().equals(contactId))
                .sorted(Comparator.comparing(f -> f.getTime())).collect(Collectors.toList()));

        if (dossier.getFile().size() > 0) {
            return new ResponseEntity<>(modelMapper.map(dossier, DossierDTO.class), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/addFileToDossier")
    public ResponseEntity addFileToDossier(@RequestHeader("User-ID") Long userId, @RequestBody FileDTO fileDTO){
        dossierService.addFileToDossier(userId, modelMapper.map(fileDTO, File.class));
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @PutMapping("/updateFile")
    public ResponseEntity updateFile(@RequestBody FileDTO fileDTO) {
        fileService.updateFile(modelMapper.map(fileDTO, File.class));
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @PutMapping("/deleteNoteFromDossierFile/{dossierId}/{fileId}")
    public ResponseEntity deleteFile(@PathVariable Long dossierId, @PathVariable Long fileId) {
        dossierService.deleteNoteFromDossierFile(dossierId, fileId);
        return new ResponseEntity(HttpStatus.OK);
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<DossierDTO> deleteDossier(@PathVariable Long id) {
        dossierService.deleteDossier(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
