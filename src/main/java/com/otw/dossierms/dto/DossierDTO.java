package com.otw.dossierms.dto;

import com.otw.dossierms.model.File;

import java.util.List;

public class DossierDTO {
    private Long contactId;
    private List<File> file;

    public DossierDTO(Long contactId, List<File> file) {
        this.contactId = contactId;
        this.file = file;
    }

    public DossierDTO() {
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public List<File> getFile() {
        return file;
    }

    public void setFile(List<File> file) {
        this.file = file;
    }
}
