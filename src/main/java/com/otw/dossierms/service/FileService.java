package com.otw.dossierms.service;

import com.otw.dossierms.model.File;
import com.otw.dossierms.repository.FileRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class FileService {
    private final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public File save(File file) {
        return fileRepository.save(file);
    }

    public List<File> findAll() {
        return fileRepository.findAll();
    }

    public void updateFile(File file) {
        Optional<File> prevfile = fileRepository.findById(file.getId());
        if (prevfile.isPresent()) {
            File fileToChange = prevfile.get();
            fileToChange.setTitle(file.getTitle());
            fileToChange.setNote(file.getNote());
            fileRepository.save(fileToChange);
        }
    }

    public void deleteFile(Long fileId) {
        fileRepository.deleteById(fileId);
    }

}
