package com.otw.dossierms.service;

import com.otw.dossierms.dto.FileDTO;
import com.otw.dossierms.model.Dossier;
import com.otw.dossierms.model.File;
import com.otw.dossierms.repository.DossierRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DossierService {
    private final DossierRepository dossierRepository;

    public DossierService(DossierRepository dossierRepository) {
        this.dossierRepository = dossierRepository;
    }

    public Dossier save(Dossier dossier) {
        return dossierRepository.save(dossier);
    }

    public List<Dossier> findAll() {
        return dossierRepository.findAll();
    }

    public Dossier findByContactId(Long contactId) {
        Optional<Dossier> dossier = dossierRepository.findByContactId(contactId);
        return dossier.orElse(null);
    }

    public Optional<Dossier> findDossier(Long id) {
        return dossierRepository.findById(id);
    }

    public Dossier update(Long id, Long contact_id, List<File> files) {
        Dossier dossier = dossierRepository.getOne(id);
        dossier.setContactId(contact_id);
        dossier.setFile(files);
        return this.save(dossier);
    }

    public void addFileToDossier(Long userId, File file) {
        Optional<Dossier> dossier = dossierRepository.findByContactId(userId);

        if (file.getTime() == null){
            file.setTime(LocalDateTime.now());
        }

        if (!dossier.isPresent()) {
            dossierRepository.save(new Dossier(userId, Arrays.asList(file)));
        } else {
            dossier.get().getFile().add(file);
        }
    }

    public void deleteDossier(Long id) {
        dossierRepository.deleteById(id);
    }

    public void deleteAll() {
        dossierRepository.deleteAll();
    }

    public void deleteNoteFromDossierFile(Long dossierId, Long fileId) {
        Optional<Dossier> dossierToUpdate = dossierRepository.findById(dossierId);
        if (dossierToUpdate.isPresent()) {
            Dossier dossier = dossierToUpdate.get();
            Optional<File> file = dossier.getFile().stream()
                    .filter(f -> f.getId().equals(fileId)).findFirst();

            if (file.isPresent()) {
                file.get().setNote("");

                this.save(dossier);
            }
        }
    }
}
