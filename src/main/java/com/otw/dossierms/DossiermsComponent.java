package com.otw.dossierms;

import com.otw.dossierms.model.Dossier;
import com.otw.dossierms.model.File;
import com.otw.dossierms.service.DossierService;
import com.otw.dossierms.service.FileService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Arrays;

@Component
public class DossiermsComponent implements CommandLineRunner {
    private DossierService dossierService;
    private FileService fileService;

    public DossiermsComponent(DossierService dossierService, FileService fileService) {
        this.dossierService = dossierService;
        this.fileService = fileService;
    }

    @Override
    public void run(String... args) throws Exception {
        dossierService.deleteAll();
        File file = new File(2L, LocalDateTime.of(2017, 4, 20, 5, 55), 4L, 49L, "KdG Stagemoment", "- Veel aangesproken - Duizend jobaanbiedingen");
        File file2 = new File(2L, LocalDateTime.of(2018, 4, 20, 5, 55), 4L, 49L, "Gesprek digipolis", "- Keuze opdracht");
        File file3 = new File(3L, LocalDateTime.of(2018, 4, 20, 5, 55), 4L, 49L, "Jonas zijn contactmomentje", "Niks aan");
        Dossier dossier = new Dossier(1L, Arrays.asList(file, file2,file3));
        dossierService.save(dossier);
    }
}
